import moment from "moment";

export function CSVToMatrix(csv) {
    let matrix = [];
    csv.split('\n').map( line => {
        line.trim() == '' ? 0 : matrix.push(
            (line.trim().split(',').map(value => value.trim())).filter(element => {
                // if empty string or Date don't parse
                if (!(element === '' || moment(element).isValid())) {
                    return element;
                }
            })
        );
    });
    return matrix.filter(Boolean);
}

export function MatrixToJSON(matrix,from,to){
    let jsonResult = []; from = from||0;
    matrix.map((a,i) => {
        let obj = Object.assign({}, ...matrix[0].map((h, index) => ({[h]: matrix[i][index]})));
        jsonResult.push(obj)
    });
    return to ? jsonResult.splice(from,to) : jsonResult.splice(from)
}
