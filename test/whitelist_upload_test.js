import CONFIG from '../config/test_config';
import LoginPage from '../pages/login/LoginPage'
import LandingPage from '../pages/landing/LandingPage';
import { expect } from 'chai';
import ManageWhitelistsPage from "../pages/manage_whitelists/ManageWhitelistsPage";
import fs from 'fs';
import path from 'path';
import { CSVToMatrix, MatrixToJSON } from '../utils/csvMatrixParser';
import _  from 'lodash';

describe('Whitelist Upload', () => {
    before('log in', () => {
        LoginPage.open();
        LoginPage.login(CONFIG.VALID_USER_CREDENTIALS.USERNAME, CONFIG.VALID_USER_CREDENTIALS.PASSWORD);
        LandingPage.toolbarButton.click();
        LandingPage.whitelistButton.click();
    });

    it('has uploaded the whitelist correctly', () => {
        ManageWhitelistsPage.createWhitelist();
    });

    it('has matching values when the whitelist is inspected', () => {
        // local CSV processing
        const csv = fs.readFileSync(path.resolve(__dirname, '../data/whitelist.csv'), 'utf8');
        const csvMatrixForComparison = CSVToMatrix(csv);
        const idSetForLocators = _.map(_.drop(MatrixToJSON(csvMatrixForComparison)), 'Provider ID');

        // Fetch whitelist data from page via Selenium
        ManageWhitelistsPage.fetchWhitelistDataRowFromPage(idSetForLocators);
        const webDataArray = Array.from(ManageWhitelistsPage.tableRowDataMap.values());

        // Comparison and assertion
        expect(_.isEqual(_.drop(csvMatrixForComparison), webDataArray)).to.be.true;
    });
});


