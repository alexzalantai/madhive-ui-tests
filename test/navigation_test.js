import CONFIG from '../config/test_config';
import LoginPage from '../pages/login/LoginPage'
import LandingPage from '../pages/landing/LandingPage';
import { expect } from 'chai';
import ManageWhitelistsPage from "../pages/manage_whitelists/ManageWhitelistsPage";

describe('Navigation', () => {
    beforeEach('log in', () => {
        LoginPage.open();
        LoginPage.login(CONFIG.VALID_USER_CREDENTIALS.USERNAME, CONFIG.VALID_USER_CREDENTIALS.PASSWORD);
    });

    it('can navigate to Manage Whitelists page', () => {
        LandingPage.toolbarButton.click();
        LandingPage.whitelistButton.click();
        expect(ManageWhitelistsPage.pageTitle.waitForDisplayed(2000)).to.be.true;
        expect(ManageWhitelistsPage.tableHeader.waitForDisplayed(2000)).to.be.true;
    });

});


