import LoginPage from '../pages/login/LoginPage'
import TrackingPixelsPage from '../pages/landing/LandingPage';
import CONFIG from '../config/test_config';
import { expect } from 'chai';
import faker from 'faker';

describe('Login to site', () => {
    it('can login with valid credentials', () => {
        LoginPage.open();
        LoginPage.login(CONFIG.VALID_USER_CREDENTIALS.USERNAME, CONFIG.VALID_USER_CREDENTIALS.PASSWORD);
        expect(TrackingPixelsPage.profileButton.waitForDisplayed(5000)).to.be.true;
    });

    it('can not login with invalid credentials', () => {
        LoginPage.open();
        LoginPage.login(faker.internet.email(), faker.internet.password());
        expect(LoginPage.errorText.waitForDisplayed(5000)).to.be.true;
    });
});


