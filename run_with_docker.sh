#!/usr/bin/env bash
docker network create selenium_n || true \
&& docker rm -f selenium chrome || true \
&& docker run -d --network selenium_n --name chrome -p 4444:4444 -p 5900:5900 -v /dev/shm:/dev/shm selenium/standalone-chrome-debug:3.141.59-xenon \
&& docker build . -t selenium \
&& docker run --name selenium --network selenium_n -e REMOTE_HOST=chrome selenium npm run test:ci \
&& docker logs -f selenium
