exports.config = {
    runner: 'local',
    path: '/',
    specs: ['./test/**/login_test.js'],
    capabilities: [{
        maxInstances: 1,
        browserName: 'chrome',
    }],
    logLevel: 'trace',
    waitforTimeout: 10000,
    connectionRetryCount: 3,
    services: ['chromedriver'],
    framework: 'mocha',
    mochaOpts: {
        ui: 'bdd',
        require: ['@babel/register']
    },
    reporters: ['spec'],
};
