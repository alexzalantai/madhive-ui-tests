// Tracking Pixels Page
export const H_PIXELS = 'h3=pixels';
export const BTN_PROFILE = '[data-testid="profile-button"]';
// Seems like the click target is off with this one, so had to use the class instead, see below.
// export const BTN_TOOLBAR = '[data-testid="toolbar-uat-media"]';
export const BTN_TOOLBAR = '.MuiIconButton-label';
export const BTN_WHITELIST = 'div=Manage Whitelists';
