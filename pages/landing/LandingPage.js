import Page from '../Page';
import * as locators from './LandingPageLocators';

class LandingPage extends Page {
    get profileButton() { return $(locators.BTN_PROFILE); }
    get pixelsHeading() { return $(locators.H_PIXELS); }
    get toolbarButton() { return $(locators.BTN_TOOLBAR); }
    get whitelistButton() { return $(locators.BTN_WHITELIST); }
}

export default new LandingPage();
