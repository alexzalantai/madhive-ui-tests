import Page from '../Page';
import uuidv4 from 'uuid/v4';
import * as locators from './ManageWhitelistsElement';
import path from 'path';
import moment from "moment";

class ManageWhitelistsPage extends Page {
    constructor() {
        super();
        this.whitelistId = uuidv4();
        this.tableRows = [];
    }

    get pageTitle() { return $(locators.H_TITLE); };
    get tableHeader() { return $(locators.H_TABLE_HEADER); };
    get createWhitelistButton() { return $(locators.BTN_CREATE_WHITELIST); }
    get whitelistName() { return $(locators.TXT_WHITELIST_NAME); }
    get uploadWhitelistButton() { return $(locators.BTN_UPLOAD_WHITELIST); }
    get browseFilesButton() { return $(locators.BTN_BROWSE_FILES); }
    get searchBox() { return $(locators.TXT_SEARCH); }
    get whitelistIdLabel() { return $(`span=${this.whitelistId}`); }
    get tableRowDataMap() { return this.tableRows; }
    get saveButton() { return $(locators.BTN_SAVE); }
    get uploadFinishedButton() { return $(locators.BTN_UPLOAD_FINISHED); }
    get fileUploadElement() { return $(locators.FILE_UPLOAD_SE); }

    whitelistTableRowElement(rowId) { return $$(`//div[@data-publisher-id="${rowId}"]/descendant::*`); }

    createWhitelist() {
        this.createWhitelistButton.waitForClickable();
        this.createWhitelistButton.click();
        this.whitelistName.setValue(this.whitelistId);
        this.uploadWhitelistButton.click();
        browser.execute(`document.getElementsByClassName('${locators.FILE_UPLOAD}')[0].style.display = 'block'`);
        this.fileUploadElement.waitForDisplayed();
        this.fileUploadElement.setValue(path.join(__dirname, '../../data/whitelist.csv'));
        this.uploadFinishedButton.click();
        this.saveButton.waitForClickable();
        this.saveButton.click();
    }

    fetchWhitelistDataRowFromPage(idSet) {
        this.searchBox.setValue(this.whitelistId);
        this.whitelistIdLabel.click();
        idSet.forEach ( (rowId, index) => {
            this.tableRows[index] = [rowId];
            this.whitelistTableRowElement(rowId).forEach( element => {
                if(!moment(element.getHTML(false)).isValid()) {
                    this.tableRows[index].push(element.getHTML(false));
                }
            });
        });
    }
}

export default new ManageWhitelistsPage();
