// Could move this into one hashmap or any other structure, for simplicity I've used constants in this task

// Login Page
export const TEXT_EMAIL = '#email';
export const TEXT_PASSWORD = '#password';
export const BTN_LOGIN = '[data-testid="login-button"]';
export const TEXT_ERROR = '[data-testid="error-text"]';
