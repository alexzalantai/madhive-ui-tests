import Page from '../Page';
import CONFIG from '../../config/test_config';
import * as locators from './LoginPageLocators';

class LoginPage extends Page {
    get email() { return $(locators.TEXT_EMAIL); }
    get password() { return $(locators.TEXT_PASSWORD); }
    get submitBtn() { return $(locators.BTN_LOGIN); }
    get defaultElement() { return this.email; }
    get errorText () { return $(locators.TEXT_ERROR) };

    open() {
        // to clear previous sessions
        if (browser.getUrl() !== 'data:,') {
            browser.execute('await indexedDB.deleteDatabase("firebaseLocalStorageDb");');
        }
        super.open(CONFIG.HOME_URL);
    }

    login(username, password) {
        this.email.setValue(username);
        this.password.setValue(password);
        this.submitBtn.click();
    }
}

export default new LoginPage();
