exports.config = {
    runner: 'local',
    hostname: `${process.env.REMOTE_HOST}`,
    port: 4444,
    path: '/wd/hub',
    capabilities: [{
        browserName: 'chrome',
    }],
    specs: ['./test/**/*_test.js'],
    logLevel: 'silent',
    waitforTimeout: 10000,
    connectionRetryCount: 3,
    framework: 'mocha',
    mochaOpts: {
        ui: 'bdd',
        require: ['@babel/register']
    },
    reporters: ['spec'],
};
