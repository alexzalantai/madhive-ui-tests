# Madhive UI Test Task

Author: Alex Zalantai <br>
High level summary: <br>
Madhive QA task completion.
Tests are atomic, and repeatable any number of times. 

High level structure:
![](code_structure.png)

## Setup<br>
* Have Node.js (v12) and npm installed (nvm advised as well)
* Have a recent JRE installed as it can be a dependency for the some of the drivers

## Run the tests locally<br>
There are two options to run the tests locally, either natively or in Docker. 2 sets of steps to follow below:
Native:
* `npm i && npm test:local` <br>
Docker:
* Have Docker installed on the machine
* Spin a network for the containers so they can talk to each other easily:
`docker network create selenium_n`
* Spin a container for the browser (with VNC so can debug): <br>
`docker run -d --network selenium_n --name chrome -p 4444:4444 -p 5900:5900 -v /dev/shm:/dev/shm selenium/standalone-chrome-debug:3.141.59-xenon`
* Kick off the tests:
`docker build . -t selenium && docker run --name selenium --network selenium_n -e REMOTE_HOST=chrome selenium npm run test:ci` 
* Connect to the VNC port(5900) with any VNC client. Password is pre-set to `secret`

## Patterns<br>
* Page Object Model - this is a very common model to represent pages in code for maintainability
* Elements/Locators/Pages - depending on the size of a test pack, it can be a good idea to separate these concerns into their own files
* Factories - not used in this exercise, but can be very useful when working with UI automation (https://www.tutorialspoint.com/design_pattern/factory_pattern.htm)
* Strategy - again, can be a very useful pattern to increase maintainability (https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm) 
## CI<br>
Why write tests if they will not be ran as part of a build? <br>
Tests need to be quick enough to run on commit hooks ideally, they need to run the same way locally as remotely.<br>
Common CI alternatives to Gitlab CI:
* Jenkins
* Travis
* TeamCity

## Narrative for tool selection and alternatives<br>
### ES6<br>
Vanilla Javascript would have been a pick that makes the test code a bit harder to read.<br/>
TypeScript is an option however, given that test code should be simple as possible for maintenance purposes, maybe its a bit of an overkill.<br/>
Other languages lend themselves to UI automation easily as well such as Python, Ruby, Java etc...<br/>
Maybe the most mature Webdriver ecosystem options are Java and Javascript, which can be useful if the tests need to do something non-standard. 
### WebdriverIO<br>
Given the simple task, WebdriverIO was an obvious pick as it has wide range of tools and integrations available. <br/>
Alternatives would be:
* vanilla selenium-webdriver - which is a good option to build something from scratch and fully customise it
* Cucumber.js - comes with Gherkin, has a new parallel runner option, and good integrations. Bit of an overshoot for this task
* Cypress.io - fairly new tool, only able to execute on chrome currently and is commercial
* Protractor - mainly useful for Angular bindings however, it matured a lot recently, this would have been a good option as well
### Mocha + Chai<br>
Two tools that I have been using lately. Not set on them, there are alternatives out there to use like:
* Jest
* Jasmine
* Karma
* Cucumber <br>
The advantage of Cucumber with the Gherkin DSL can be the reporting capabilities.
The features/scenarios are very easy to read, and can be parsed into presentable reports with something like Allure for example.
This can come very handy when customers / other stakeholders would like to receive test results as well.
### Spec Reporter<br>
Picked this reporter because its nice and simple and works really well with Mocha. <br>
If the reports need to be seen by customers / third parties it is a good idea to pick something that generates HTML and looks good with animations etc. Examples are Allure, Cucumber, Mochawesome.<br>
WebdriverIO has numerous integrations with reporters, the setup if a new reporter is a tricial task. 
### Gitlab<br>
Picked Gitlab because of the CI offering. Easy to handle and matured a lot in the recent years.
### Docker<br>
Containerisation of the work makes it portable. Can be executed on CI efficiently, can be transferred to other machines which have docker installed - regardless the OS. 
## Next Steps<br>
* Add multi-browser capability to run across mainstream browsers.
 Could use a tool like Browserstack/Saucelabs or just build Chrome/Firefox images with the available stock Selenium docker images
* I have not used factories, as for this task it is an unnecessary over-complication.
 As a test pack grows and execution time increases, it can be a good idea to run them in parallel.
 To support this a browser factory and a page factory can be created, and this can run on a dockerized grid.
* Appium support. WebdriverIO has support for this out of the box however, all other frameworks can have an Appium driver added as well.
* Consider adding aux tools depending on requirements: accessibility scanner, security scanner etc.
* Tidy up that nasty moment console eyesore with some better method of detecting whether a string is a parsable date or not 
# Sources<br>
https://webdriver.io/
https://github.com/SeleniumHQ/docker-selenium
https://gist.github.com/Jonarod/b971b2df24ba46c33c37afb2a1dcb974

